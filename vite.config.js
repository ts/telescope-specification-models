import { defineConfig } from 'vite'
import { resolve } from 'path'
import dts from 'vite-plugin-dts'
import * as packageJson from "./package.json";

export default defineConfig({
  resolve: {
    alias: {
    }
  },
  test: {
    globals: true,
    coverage: {
      provider: "v8",
      reporter: ["text", "lcov", "html"],
      include: ["lib/models/**/*.ts"], // Ensure all relevant files are included
      all:true,
    },
  },
  plugins: [dts({
    insertTypesEntry: true,  
      include: ['lib'] ,
       
    
  })],
    build: {
        rollupOptions: {
          external: Object.keys(packageJson.peerDependencies),
           },
      copyPublicDir:false,
        lib: {
          entry: resolve('lib/main.ts'),
          formats: ["es", "cjs"],
        fileName: (format) =>
        `telescope-specification-models.${format === "cjs" ? "cjs" : "es.js"}`,
    
        },
      minify: false  
      }
})



