import { describe, it, expect } from 'vitest';
import PriorityQueue from '../../models/tmss/PriorityQueue'; // Adjust the import path as necessary

describe('PriorityQueue', () => {
  it('should have a static property PriorityQueueA with value "A"', () => {
    expect(PriorityQueue.PriorityQueueA).toBe("A");
  });

  it('should have a static property PriorityQueueB with value "B"', () => {
    expect(PriorityQueue.PriorityQueueB).toBe("B");
  });

  it('should have a static property AllQueues containing ["A", "B"]', () => {
    expect(PriorityQueue.AllQueues).toEqual(["A", "B"]);
  });
});
