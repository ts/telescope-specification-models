import { describe, it, expect } from 'vitest';
import BaseTMSSTemplate from '../../models/tmss/BaseTmssTemplate';  

const validTemplate: BaseTMSSTemplate = {
  id: 'template1',
  url: 'https://tmss.lofar.eu/lla',
  description: 'A sample template',
  name: 'Template One',
  version: '1.0.0',
};


describe('BaseTMSSTemplate', () => {
  it('should conform to BaseTMSSTemplate interface', () => {
    expect(validTemplate).toEqual({
      id: 'template1',
      url: 'https://tmss.lofar.eu/lla',
      description: 'A sample template',
      name: 'Template One',
      version: '1.0.0',
    });
  });


});
