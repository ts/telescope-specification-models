import CommonSchemaTemplate from '../../models/tmss/CommonSchemaTemplate';  
import { describe, it, expect } from 'vitest';
import BaseTMSSTemplate from '../../models/tmss/BaseTmssTemplate';  

const validTemplate: CommonSchemaTemplate = {
  id: 'template1',
  url: 'https://tmss.lofar.eu/lla',
  description: 'A sample template',
  name: 'Template One',
  version: '1.0.0',
};


describe('BaseTMSSTemplate', () => {
  it('should conform to BaseTMSSTemplate interface', () => {
    const isBaseTMSSTemplate: BaseTMSSTemplate = validTemplate;
    expect(isBaseTMSSTemplate).toEqual({
      id: 'template1',
      url: 'https://tmss.lofar.eu/lla',
      description: 'A sample template',
      name: 'Template One',
      version: '1.0.0',
    });
  });


});
