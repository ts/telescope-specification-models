import { describe, it, expect } from 'vitest';
import Cycle from "../../models/tmss/Cycle";

describe('Cycle Interface', () => {

    const cycle: Cycle = {
        name: 'Cycle 1',
        url: 'http://example.com',
        created_at: '2023-01-01T00:00:00Z',
        updated_at: '2023-01-02T00:00:00Z',
        description: 'This is a test cycle.',
        duration: 30,
        project: ['Project 1', 'Project 2'],
        project_ids: ['1', '2'],
        start: '2023-01-01T00:00:00Z',
        stop: '2023-01-31T23:59:59Z',
        tags: ['tag1', 'tag2'],
        quota: ['Quota 1', 'Quota 2'],
        qupta_ids: [1, 2]
      };
      
  it('should have correct structure', () => {
    expect(cycle).toHaveProperty('name');
    expect(cycle).toHaveProperty('url');
    expect(cycle).toHaveProperty('created_at');
    expect(cycle).toHaveProperty('updated_at');
    expect(cycle).toHaveProperty('description');
    expect(cycle).toHaveProperty('duration');
    expect(cycle).toHaveProperty('project');
    expect(cycle).toHaveProperty('project_ids');
    expect(cycle).toHaveProperty('start');
    expect(cycle).toHaveProperty('stop');
    expect(cycle).toHaveProperty('tags');
    expect(cycle).toHaveProperty('quota');
    expect(cycle).toHaveProperty('qupta_ids');
  });

  it('should have correct types', () => {
    expect(typeof cycle.name).toBe('string');
    expect(typeof cycle.url).toBe('string');
    expect(typeof cycle.created_at).toBe('string');
    expect(typeof cycle.updated_at).toBe('string');
    expect(typeof cycle.description).toBe('string');
    expect(typeof cycle.duration).toBe('number');
    expect(Array.isArray(cycle.project)).toBe(true);
    expect(Array.isArray(cycle.project_ids)).toBe(true);
    expect(typeof cycle.start).toBe('string');
    expect(typeof cycle.stop).toBe('string');
    expect(Array.isArray(cycle.tags)).toBe(true);
    expect(Array.isArray(cycle.quota)).toBe(true);
    expect(Array.isArray(cycle.qupta_ids)).toBe(true);
  });
});
