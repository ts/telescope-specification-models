import { describe, it, expect } from 'vitest';
import ScheduleSpecification from '../../models/tmss/ScheduleSpecification';

describe('ScheduleSpecification Interface', () => {


  it('should create a valid ScheduleSpecification object with optional scheduling_constraints_template_id', () => {
    const schedule: ScheduleSpecification = {
      name: 'Test Schedule',
      description: 'A description for the test schedule',
      scheduling_constraints_template_id: '12345'
    };

    expect(schedule.name).toBe('Test Schedule');
    expect(schedule.description).toBe('A description for the test schedule');
    expect(schedule.scheduling_constraints_template_id).toBe('12345');
  });

  it('should allow scheduling_constraints_template_id to be undefined', () => {
    const schedule: ScheduleSpecification = {
      name: 'Test Schedule',
      description: 'A description for the test schedule',
      scheduling_constraints_template_id: undefined
    };
    expect(schedule.scheduling_constraints_template_id).toBeUndefined();
  });

});
