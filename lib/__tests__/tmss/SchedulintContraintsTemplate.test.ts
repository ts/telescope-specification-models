import { describe, it, expect } from 'vitest';
import { SchedulingConstraintsTemplate, RefResolvedSchema, SchedulingConstraintsTemplateProperties, SkyProperties, SkyNestedProperties, TransitOffsetProperties, TransitOffsetNestedProperties, MinDistanceProperties, MinDistanceNestedProperties, DefaultProperty, PropertyOrder } from '../../models/tmss/SchedulingConstraintsTemplate'

// Create a mock object that adheres to the SchedulingConstraintsTemplate interface
const mockSchedulingConstraintsTemplate: SchedulingConstraintsTemplate = {
  id: 'template1',
  url: 'https://tmss.lofar.eu/template1',
  description: 'A sample scheduling constraints template',
  name: 'Scheduling Constraints Template',
  version: '1.0.0',
  ref_resolved_schema: {
    properties: {
      sky: {
        properties: {
          transit_offset: {
            properties: {
              from: { default: '00:00' },
              to: { default: '12:00' }
            }
          },
          min_distance: {
            properties: {
              sun: { propertyOrder: 1 },
              moon: { propertyOrder: 2 },
              jupiter: { propertyOrder: 3 }
            }
          }
        }
      }
    }
  }
};

describe('SchedulingConstraintsTemplate', () => {
  it('should have all required properties', () => {
    expect(mockSchedulingConstraintsTemplate).toHaveProperty('id', 'template1');
    expect(mockSchedulingConstraintsTemplate).toHaveProperty('url', 'https://tmss.lofar.eu/template1');
    expect(mockSchedulingConstraintsTemplate).toHaveProperty('description', 'A sample scheduling constraints template');
    expect(mockSchedulingConstraintsTemplate).toHaveProperty('name', 'Scheduling Constraints Template');
    expect(mockSchedulingConstraintsTemplate).toHaveProperty('version', '1.0.0');

    const refResolvedSchema = mockSchedulingConstraintsTemplate.ref_resolved_schema;
    expect(refResolvedSchema).toHaveProperty('properties');

    const schedulingProperties = refResolvedSchema.properties;
    expect(schedulingProperties).toHaveProperty('sky');

    const skyProperties = schedulingProperties.sky.properties;
    expect(skyProperties).toHaveProperty('transit_offset');
    expect(skyProperties).toHaveProperty('min_distance');

    const transitOffset = skyProperties.transit_offset.properties;
    expect(transitOffset).toHaveProperty('from');
    expect(transitOffset).toHaveProperty('to');
    expect(transitOffset.from).toHaveProperty('default', '00:00');
    expect(transitOffset.to).toHaveProperty('default', '12:00');

    const minDistance = skyProperties.min_distance.properties;
    expect(minDistance).toHaveProperty('sun');
    expect(minDistance).toHaveProperty('moon');
    expect(minDistance).toHaveProperty('jupiter');
    expect(minDistance.sun).toHaveProperty('propertyOrder', 1);
    expect(minDistance.moon).toHaveProperty('propertyOrder', 2);
    expect(minDistance.jupiter).toHaveProperty('propertyOrder', 3);
  });
});
