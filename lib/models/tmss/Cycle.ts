
export interface Cycle {

    name:string;
    url:string;
    created_at:string;
    updated_at:string;
    description:string;
    duration:number;
    project?:string[];
    project_ids?:string[];
    start:string;
    stop:string;
    tags:string[];
    quota:string[];
    qupta_ids:number[];
}

export default Cycle;