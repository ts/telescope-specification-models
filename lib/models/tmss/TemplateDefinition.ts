
// Starting point for Broader moddeling the specification Definiation (Combination of task and constraints)
export interface TemplateDefinition {
    [key: string]: unknown;
  }

  export default TemplateDefinition;