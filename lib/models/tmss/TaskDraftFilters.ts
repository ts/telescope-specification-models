export interface TaskDraftList {
  name: string;
  description: string;
  renders: string[];
  parses: string[];
  filters: TaskDraftListFilters;
  ordering: string[];
}

export interface TaskDraftListFilters {
  [key: string]: TaskDraftListFilter;
}

export interface TaskDraftListFilter {
  type: string;
  lookup_types: string[];
}

export default TaskDraftList;
