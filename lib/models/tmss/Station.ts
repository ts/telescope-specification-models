export interface MultiStationConfig {
    [key: string]: Array<string>;
}

export interface CustomStationConfig extends StationGroup{
    error:boolean;
}

export interface CustomgStationCheck {
    [key: string]: StationsDataCustom
}


export interface StationsDataCustom {
    [key: string]: CustomStationConfig;
}



export interface MissingStationCheck {
    [key: string]: StationsData
}


export interface StationGroup {
    stations: Array<string>;
    max_nr_missing?: number;
}




export interface StationsData {
    [key: string]: StationGroup;
}




export interface StationGroupErrorState {
    [key: string]: number
}

