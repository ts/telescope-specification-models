export class PriorityQueue {
    static readonly PriorityQueueA = "A";
    static readonly PriorityQueueB = "B";
    static readonly AllQueues = [PriorityQueue.PriorityQueueA, PriorityQueue.PriorityQueueB];
  }

  export default PriorityQueue
