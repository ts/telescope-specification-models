import {BaseTMSSTemplate} from "./BaseTmssTemplate";

export interface TaskTemplate extends BaseTMSSTemplate {}

export default TaskTemplate;
