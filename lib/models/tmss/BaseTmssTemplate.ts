export interface BaseTMSSTemplate {
  id: string;
  url: string;
  description: string;
  name: string;
  version: string;
}


export default BaseTMSSTemplate