import {BaseTMSSTemplate} from "./BaseTmssTemplate";
import SchedulingConstraintsTemplate, { SchedulingConstraintsTemplateProperties } from "./SchedulingConstraintsTemplate";

export interface SchedulingUnitObservingStrategy extends BaseTMSSTemplate {
  purpose_value: string;
  scheduling_unit_template: string;
  scheduling_unit_template_id: number;
  state_value: string;
  template?: Template; // Update this line
}


export interface Template {
  scheduling_constraints_template?: SchedulingConstraintsTemplate;
  scheduling_constraints_doc?: SchedulingConstraintsTemplateProperties;
  tasks: { [key: string]: SchedulingUnitObservingStrategyDoc };
  parameters: SchedulingUnitObservingStrategyRefs[];
}

export interface SchedulingUnitObservingStrategyRefs {
  refs: string[];
  name: string;
}

export interface SchedulingUnitObservingStrategyDoc {
  specifications_doc: SpecificationDoc;
}

export interface SpecificationDoc {
  station_configuration: { [key: string]: TemplateStringsArray };
}

export default SchedulingUnitObservingStrategy;
