import {BaseTMSSTemplate} from "./BaseTmssTemplate";

export interface CommonSchemaTemplate extends BaseTMSSTemplate {}

export default CommonSchemaTemplate;
