import {BaseTMSSTemplate} from "./BaseTmssTemplate";

export interface SchedulingConstraintsTemplate extends BaseTMSSTemplate {
  ref_resolved_schema: RefResolvedSchema;
}

export default SchedulingConstraintsTemplate;

export interface RefResolvedSchema {
  properties: SchedulingConstraintsTemplateProperties;
}

export interface SchedulingConstraintsTemplateProperties {
  sky: SkyProperties;
}

export interface SkyProperties {
  properties: SkyNestedProperties;
}

export interface SkyNestedProperties {
  transit_offset: TransitOffsetProperties;
  min_distance: MinDistanceProperties;
}

export interface TransitOffsetProperties {
  properties: TransitOffsetNestedProperties;
}

export interface TransitOffsetNestedProperties {
  from: DefaultProperty;
  to: DefaultProperty;
}

export interface MinDistanceProperties {
  properties: MinDistanceNestedProperties;
}

export interface MinDistanceNestedProperties {
  sun: PropertyOrder;
  moon: PropertyOrder;
  jupiter: PropertyOrder;
}

export interface DefaultProperty {
  default: string;
}
export interface PropertyOrder {
  propertyOrder: number;
}
