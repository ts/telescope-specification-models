export interface TaskParamSchema {
  type: string;
  additionalProperties: boolean;
  properties: { [key: string]: TaskParamSchemaProperties };
  definitions: TaskParamSchemaDefinitions;
}

export interface TaskParamSchemaProperties {
  default?: unknown | string;
  type?: string;
  title?: string;
}

export interface TaskParamSchemaDefinitions {}

export default TaskParamSchema;
