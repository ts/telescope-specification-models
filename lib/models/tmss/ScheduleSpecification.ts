export interface ScheduleSpecification {
  name: string;
  description: string;
  scheduling_constraints_template_id?: string;
}
export default ScheduleSpecification;
