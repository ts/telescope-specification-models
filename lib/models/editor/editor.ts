
export interface EditorError {
    errorcount: number;
    message: string;
    path: string;
    property: string;
  }
        
  export default EditorError;
  