import EditorError from "./editor";

export interface SpecificationDetailsFormStatus {
    isValidTaskEditor: boolean;
    isValidConstraintsEditor: boolean;
    taskErrors?: EditorError[];
  } 
  
  export default SpecificationDetailsFormStatus;
  